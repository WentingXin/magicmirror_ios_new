//
//  Internet.m
//  Photo_Eday
//
//  Created by dongl on 14-9-11.
//  Copyright (c) 2014年 dongl. All rights reserved.
//

#import "Internet.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
@implementation Internet
+(NSInteger)CheckInternet{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            [SVProgressHUD showErrorWithStatus:@"当前没有网络"];
            NSLog(@"当前没有网络");
            break;
        case ReachableViaWWAN:
            NSLog(@"正在使用3G网络");
            break;
        case ReachableViaWiFi:
            NSLog(@"正在使用WiFi网络");
            break;
    }
    return [r currentReachabilityStatus];
    
}

@end
